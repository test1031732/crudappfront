import '../App.css';
import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';

const EditPerson = () => {
    const navigate = useNavigate();
    const { id } = useParams();
    const [formData, setFormData] = useState({
        firstName: '',
        lastName: '',
        email: ''
    });
    const [error, setError] = useState('');

    useEffect(() => {
        async function fetchPerson() {
            await fetch(`${process.env.REACT_APP_API_URL}/persons/${id}`)
                .then((response) => response.json())
                .then(data => setFormData(data))
                .catch((err) => {
                    console.error(err);
                    alert("An error occured!");
                });
        }
        fetchPerson();
    }, [id]);

    const handleCancel = () => {
        navigate('/');
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setError('');

        const requestOptions = {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(formData)
        };
        const response = await fetch(`${process.env.REACT_APP_API_URL}/persons/${id}`, requestOptions)
            .then(async response => {
                const data = await response.json();

                // check for error response
                if (!response.ok) {
                    // get error message from body or default to response status
                    const error = (data && data.message) || response.status;
                    setError(error);
                } else {
                    navigate('/');
                }
            })
    };

    return (
        <div className="edit-container">
            <h2>Edit Person</h2>
            {error && <p className="error-message" variant="danger">{error}</p>}
            <form onSubmit={handleSubmit}>
                <div className='form-group'>
                    <label htmlFor='firstName'>First Name</label>
                    <input id='firstName' type='text' name='firstName' required
                        value={formData.firstName} onChange={handleInputChange} />
                </div>

                <div className='form-group'>
                    <label htmlFor='lastName'>Last Name</label>
                    <input id='lastName' type='text' name='lastName' required
                        value={formData.lastName} onChange={handleInputChange} />
                </div>
                <div className='form-group'>
                    <label htmlFor='email'>Email</label>
                    <input id='email' type='email' name='email' required
                        value={formData.email} onChange={handleInputChange} />
                    {formData.email && !/\S+@\S+\.\S+/.test(formData.email) && (
                        <p className="error-message">Please enter a valid email address.</p>
                    )}
                </div>
                <button type='submit'>Save</button>
                <button onClick={handleCancel}>Cancel</button>
            </form>
        </div>
    );
}
export default EditPerson;