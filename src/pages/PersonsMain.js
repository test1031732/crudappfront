import '../App.css';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const PersonsMain = () => {
    const navigate = useNavigate();
    const [refreshFlag, setRefreshFlag] = useState(0);
    const [persons, setPersons] = useState([]);
    const [sortOrder, setSortOrder] = useState('asc');
    const [sortedPersons, setSortedPersons] = useState([]);

    const fetchData = async () => {
        await fetch(`${process.env.REACT_APP_API_URL}/persons`)
            .then((response) => response.json())
            .then(data => setPersons(data))
            .catch((err) => {
                console.error(err);
                alert("An error occured!");
            });
    };

    useEffect(() => {
        fetchData();
    }, []);

    const toggleSortOrder = () => {
        const newSortOrder = sortOrder === 'asc' ? 'desc' : 'asc';
        setSortOrder(newSortOrder);
    };

    useEffect(() => {
        if (persons.length > 0) {
            const sorted = [...persons];
            sorted.sort((a, b) => {
                const lastNameA = a.lastName.toLowerCase();
                const lastNameB = b.lastName.toLowerCase();
                if (sortOrder === 'asc') {
                    return lastNameA.localeCompare(lastNameB);
                } else {
                    return lastNameB.localeCompare(lastNameA);
                }
            });
            setSortedPersons(sorted);
        }
    }, [persons, sortOrder]);


    const handleDelete = async (id) => {
        const confirmDelete = window.confirm('Are you sure you want to delete this person?');
        if (confirmDelete) {
            try {
                await fetch(`${process.env.REACT_APP_API_URL}/persons/${id}`, { method: 'DELETE' });
                fetchData();
                setRefreshFlag(refreshFlag + 1);
            } catch (error) {
                console.error('Error deleting person:', error);
            }
        }
    };

    const handleEdit = (id) => {
        navigate(`/edit/${id}`);
    };

    if (persons) {
        return (
            <table>
                <thead>
                    <tr>
                        <th onClick={toggleSortOrder} style={{ cursor: 'pointer' }}>
                            Last Name {sortOrder === 'asc' ? '▲' : '▼'}
                        </th>
                        <th>First Name</th>
                        <th>Email</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {sortedPersons.map(person => (
                        <tr key={person.id}>
                            <td>{person.lastName}</td>
                            <td>{person.firstName}</td>
                            <td>{person.email}</td>
                            <td>
                                <button name='edit' onClick={() => handleEdit(person.id)}>&#128221;</button>
                            </td>
                            <td>
                                <button name='delete' onClick={() => handleDelete(person.id)}>&#10060;</button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table >
        );
    }
}
export default PersonsMain;