import './App.css';
import PersonsMain from './pages/PersonsMain';
import EditPerson from './pages/EditPerson';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

const Header = () => {
  return (
    <header className='header'>
      <h1>Welcome to Crud App</h1>
    </header>
  );
};

function App() {
  return (
    <Router>
      <Header />
      <Routes>
        <Route path="/" element={< PersonsMain />} />
        <Route path="/edit/:id" element={<EditPerson />} />
      </Routes>
    </Router>
  );
}

export default App;
