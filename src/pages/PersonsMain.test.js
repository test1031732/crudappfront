import React from 'react';
import { render, screen, fireEvent, unmountComponentAtNode } from '@testing-library/react';
import '@testing-library/jest-dom';
import { MemoryRouter } from 'react-router-dom';
import App from '../App'
import { act } from 'react-dom/test-utils'; 
import PersonsMain from './PersonsMain';

describe('Persons Main Test', () => {
    test('should have an empty table', async () => {
        render(<App />);
        expect(screen.getAllByRole('table')).toHaveLength(1);
    });

    test('should toggle the sort order', () => {
        render(<App />);
        const sortOrderButton = screen.getByText('Last Name ▲');
        fireEvent.click(sortOrderButton);
        expect(sortOrderButton.textContent).toEqual('Last Name ▼');
    });

    it('should be able to inject data', async () => {
        const fakePersons = [
            { id: 1, firstName: 'John', lastName: 'Doe', email: 'john@example.com' },
            { id: 2, firstName: 'Jane', lastName: 'Smith', email: 'jane@example.com' },
        ];

        jest.spyOn(global, 'fetch').mockResolvedValue({
            json: jest.fn().mockResolvedValue(fakePersons),
        });

        await act(async () => {
            render(
                <MemoryRouter>
                    <PersonsMain />
                </MemoryRouter>
            );
        });
        fakePersons.forEach(person => {
            expect(screen.getByText(person.firstName)).toBeInTheDocument();
            expect(screen.getByText(person.lastName)).toBeInTheDocument();
            expect(screen.getByText(person.email)).toBeInTheDocument();
        });
        global.fetch.mockRestore();
    });

    it('should be able to sort data', async () => {
        const fakePersons = [
            { id: 1, firstName: 'John', lastName: 'Abc', email: 'john@example.com' },
            { id: 2, firstName: 'Jane', lastName: 'Efg', email: 'jane@example.com' },
        ];

        jest.spyOn(global, 'fetch').mockResolvedValue({
            json: jest.fn().mockResolvedValue(fakePersons),
        });

        await act(async () => {
            render(
                <MemoryRouter>
                    <PersonsMain />
                </MemoryRouter>
            );
        });

        //Test Order befor click sort
        const sortOrderButton = screen.getByText('Last Name ▲');
        const allRec = screen.getAllByRole('row');
        expect(allRec[1].textContent).toContain("AbcJohn");
        expect(allRec[2].textContent).toContain("EfgJane");

        //Click sort
        fireEvent.click(sortOrderButton);
        expect(sortOrderButton.textContent).toEqual('Last Name ▼');

        //Check order after sort
        const allRecAfterSort = screen.getAllByRole('row');
        expect(allRecAfterSort[1].textContent).toContain("EfgJane");
        expect(allRecAfterSort[2].textContent).toContain("AbcJohn");
        global.fetch.mockRestore();
    });

    it('should be able to delete', async () => {
        const fakePersons = [
            { id: 1, firstName: 'John', lastName: 'Abc', email: 'john@example.com' },
        ];

        jest.spyOn(global, 'fetch').mockResolvedValue({
            json: jest.fn().mockResolvedValue(fakePersons),
        });

        // Mock the window.confirm function
        window.confirm = jest.fn(() => true); 

        await act(async () => {
            render(
                <MemoryRouter>
                    <PersonsMain />
                </MemoryRouter>
            );
        });

        const deleteSpy = jest.spyOn(global, 'fetch').mockResolvedValue({
            json: jest.fn().mockResolvedValue({}),
        });

        await act(async () => {
            const allBtns = screen.getAllByRole('button');
            fireEvent.click(allBtns[1]);
        });

        // Assert that fetch was called with the DELETE method and correct URL
        expect(deleteSpy).toHaveBeenCalledWith(
            `${process.env.REACT_APP_API_URL}/persons/1`,
            {
                method: 'DELETE',
            }
        );
        global.fetch.mockRestore();
        window.confirm.mockRestore();
    });

    it('should be able to update', async () => {
        const fakePersons = [
            { id: 1, firstName: 'John', lastName: 'Abc', email: 'john@example.com' },
        ];

        const spy = jest.spyOn(global, 'fetch').mockResolvedValue({
            json: jest.fn().mockResolvedValue(fakePersons),
        });

        await act(async () => {
            render(<App />);
        });


        const fakeFormData = {
            id: 1,
            firstName: 'John',
            lastName: 'Doe',
            email: 'john@example.com'
        };

        jest.spyOn(global, 'fetch').mockResolvedValue({
            json: jest.fn().mockResolvedValue(fakeFormData),
        });

        await act(async () => {
            const allBtns = screen.getAllByRole('button');
            fireEvent.click(allBtns[0]);
        });
        expect(window.location.pathname).toBe('/edit/1');
        expect(screen.getByText('Edit Person')).toBeInTheDocument();

        // Check if the form fields have the correct values
        const firstNameField = screen.getByLabelText('First Name');
        const lastNameField = screen.getByLabelText('Last Name');
        const emailField = screen.getByLabelText('Email');
        expect(firstNameField.value).toBe(fakeFormData.firstName);
        expect(lastNameField.value).toBe(fakeFormData.lastName);
        expect(emailField.value).toBe(fakeFormData.email);

        fireEvent.change(firstNameField, { target: { value: 'New John' } });
        fireEvent.change(lastNameField, { target: { value: 'New Doe' } });
        fireEvent.change(emailField, { target: { value: 'new@example.com' } });

        await act(async () => {
            const editButton = screen.getByText('Save');
            fireEvent.click(editButton);
        });

        const updateSpy = jest.spyOn(global, 'fetch').mockResolvedValue({
            json: jest.fn().mockResolvedValue(fakeFormData),
        });
        // Assert that fetch was called with the DELETE method and correct URL
        expect(updateSpy).toHaveBeenCalledWith(
            `${process.env.REACT_APP_API_URL}/persons/1`, { "body": "{\"id\":1,\"firstName\":\"New John\",\"lastName\":\"New Doe\",\"email\":\"new@example.com\"}", "headers": { "Content-Type": "application/json" }, "method": "PUT" }
        );

        global.fetch.mockRestore();
        window.confirm.mockRestore();
    });
});