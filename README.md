# Structure de l'Application
L'application est structurée en plusieurs composants React qui interagissent pour fournir la fonctionnalité CRUD.

Pour les tests unitaires les injections de données fictives ont été effectuées à l'aide de la fonction `jest.spyOn` et de la méthode `mockResolvedValue` pour simuler les réponses du serveur.
## `App.js`
Le composant App.js sert de point d'entrée de l'application et configure les routes à l'aide de React Router.
Il comprend également un en-tête (Header) affichant le titre de l'application et définit les routes pour les pages principales (PersonsMain) et les pages de modification (EditPerson).

## `PersonsMain.js`
Le composant PersonsMain est responsable de l'affichage de la liste de personnes.

Le composant effectue des appels à l'API pour récupérer et mettre à jour les données. Les personnes peuvent être triées par nom de famille en cliquant sur l'en-tête de la colonne correspondante. 

De plus, les boutons "Edit" et "Delete" permettent respectivement de naviguer vers la page de modification (EditPerson) et de supprimer une personne.

## `EditPerson.js`
Le composant EditPerson gère la modification des informations d'une personne. 

Il récupère les données de la personne à partir de l'API en fonction de l'ID passé dans l'URL. Les champs de formulaire sont pré-remplis avec les données de la personne et peuvent être modifiés. 

En soumettant le formulaire, les nouvelles données sont envoyées à l'API pour mettre à jour les informations de la personne.

## `Comment exécuter l'Application`
- Installez les dépendances : npm install
- Verifier que le URL de l'API dans .env est bien celui de la backend Spring Boot 
- Démarrez l'application : npm start
- L'application devrait s'ouvrir automatiquement dans votre navigateur. Sinon, accédez à http://localhost:3000 dans votre navigateur.