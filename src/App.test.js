import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

describe('Main App Test', () => {
    test('Header should be visible', () => {
        render(<App />);
        const sortOrderButton = screen.getByRole('heading');
        expect(sortOrderButton.textContent).toEqual('Welcome to Crud App');
    });
});